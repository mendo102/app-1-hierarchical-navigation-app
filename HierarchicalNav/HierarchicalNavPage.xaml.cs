﻿using Xamarin.Forms;

namespace HierarchicalNav
{
    public partial class HierarchicalNavPage : ContentPage
    {
        public HierarchicalNavPage()
        {
            InitializeComponent();
        }

        async void OnClickedSoccerPage(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new SoccerPage("Goal!!!!!"));

        }

        async void OnClickedBoxingPage(object sender, System.EventArgs e)
        {
            bool respons = await DisplayAlert("Warning ", "You are going to Boxing Page. Would you like to proceed ", "Yes", "No");
            if (respons == true)
            {
                await Navigation.PushAsync(new BoxingPage());
            }
        }

        void OnClickedGymPage(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new GymPage());
        }
    }
}
