﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace HierarchicalNav
{
    public partial class SoccerPage : ContentPage
    {
        public SoccerPage(string msg )
        {
            InitializeComponent();
            Message.Text = msg;
        }
    }
}
